package com.company;
import java.util.Arrays;

public class Humans {
    //fields
    private String name;
    private String surname;
    private int dateOfBirth;
    private int iqLevel;
    private String[][] schedule;
    private Family family;
    //get and set
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public int getIqLevel() {
        return iqLevel;
    }
    public void setIqLevel(int iqLevel) {
        this.iqLevel = iqLevel;
    }
    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    //all fields
    public Humans(String name, String surname, int dateOfBirth, int iqLevel, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }
    //name,surname and date of birth
    public Humans(String name, String surname, int dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }
    //empty
    public Humans() {
    }
    //Methods
    public void greetPet() {
        System.out.printf("\nHello,%s", family.getPet().getNickname());
    }
    public void describePet() {
        String slyness;
        if (family.getPet().getTrickLevel() >= 50) {
            slyness = "It's very sly";
        } else {
            slyness = "It's almost not sly";
        }
        System.out.printf("\nI have a %s,it's %d years old,it %s", family.getPet().getSpecies(), family.getPet().getAge(), slyness);
    }
    //toString
    @Override
    public String toString() {
        return String.format("Human{name = %s,surname = %s, year = %d, iq = %d, schedule = %s",
                name, surname, dateOfBirth, iqLevel, Arrays.toString(schedule));
    }
}
