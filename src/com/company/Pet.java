package com.company;
import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    // get and set
    public String getSpecies() {
        return species;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    // pet's species and nickname
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    // all
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    //empty
    public Pet() {
    }
    //methods of Pet
    public void eat() {
        System.out.println("I am eating");
    }
    public void respond() {
        System.out.printf("Hello,owner.I am %s.I miss you", nickname);
    }
    public void foul() {
        System.out.println("I need to cover it up");
    }
    //toString
    @Override
    public String toString() {
        return String.format("\nPet = %s{nickname = %s,age = %d, trickLevel = %d,habits= %s", species.toUpperCase(), nickname, age, trickLevel, Arrays.toString(habits));
    }

}
