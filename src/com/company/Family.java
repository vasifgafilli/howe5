package com.company;
import java.util.Arrays;
import java.util.Objects;

    public class Family {
        // fields
        private Humans mother;
        private Humans father;
        private Humans[] children;
        private Pet pet;
        // get and set
        public Humans getMother() {
            return mother;
        }
        public void setMother(Humans mother) {
            this.mother = mother;
        }
        public Humans getFather() {
            return father;
        }
        public void setFather(Humans father) {
            this.father = father;
        }
        public Humans[] getChildren() {
            return children;
        }
        public void setChildren(Humans[] children) {
            this.children = children;
        }
        public Pet getPet() {
            return pet;
        }
        public void setPet(Pet pet) {
            this.pet = pet;
        }
        // toString
   /* @Override
    public String toString() {
        return String.format("Humane(name = %s,surname = %s,dateOfBirth = %d",mother,mother),mother.getDateOfBirth());
    }*/
        //constructor
        public Family(Humans mother, Humans father) {
            this.mother = mother;
            this.father = father;
            this.children = new Humans[]{};
        }
        //Methods
        //add child
        public void addChild(Humans child) {
            Humans[] childArray = new Humans[children.length + 1];
            for (int i = 0; i < children.length; i++) {
                childArray[i] = children[i];
            }
            childArray[children.length] = child;
            this.children = childArray;
        }
        public boolean deleteChild(int index) {
            try {
                boolean deleted = false;
                Humans[] childArray = new Humans[this.children.length - 1];
                int i = 0;
                for (int j = 0; j < childArray.length; j++) {
                    if (j != index) {
                        childArray[i++] = children[j];
                    }
                }
                this.children = childArray;
                if (index < childArray.length - 1) {
                    deleted = true;
                }
                return deleted;
            } catch (Exception exception) {
                System.out.println("You have written a wrong index!");
            }
            return false;
        }
        public boolean deleteChild(Humans child) {
            boolean deleted = false;
            Humans[] childArray = new Humans[this.children.length - 1];
            int index = 0;
            for (Humans chil : this.children) {
                if (!chil.equals(child)) {
                    childArray[index++] = chil;
                }
            }
            this.children = childArray;
            if (index < childArray.length - 1) {
                deleted = true;
            }
            return deleted;
        }
        //countFamily
        public int countFamily() {
            int count = 2;
            for (Humans child : children) {
                if (child != null) {
                    count++;
                }
            }
            System.out.println("Family members are: " + count);
            return count;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Family family = (Family) o;
            return
                    Objects.equals(getMother(), family.getMother()) &&
                            Objects.equals(getFather(), family.getFather()) &&
                            Objects.equals(getPet(), family.getPet()) &&
                            Arrays.equals(children, family.children);
        }
        @Override
        public int hashCode() {
            int result = Objects.hash(mother, father, pet);

            result = 31 * result + Arrays.hashCode(children);

            return result;
        }
        @Override
        public String toString() {
            return "Family{" + "mother=" + mother + ", father=" + father + ", children=" + Arrays.toString(children) + ", pet=" + pet + '}';
        }
    }